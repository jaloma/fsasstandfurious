const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const entry  = "./src/index.scss";

const webpackModule = {
    rules: [
        {
            test: /\.s[ac]ss$/i,
            use: [
                {
                    loader: MiniCssExtractPlugin.loader,
                },
                {
                    // Interprets CSS
                    loader: "css-loader"
                },
                {
                    loader: "sass-loader",
                },
            ],
        },
    ],
}

const plugin = [
    // Where the compiled SASS is saved to
    new MiniCssExtractPlugin({
        filename: "main.css",
        chunkFilename: "[id].css",
    }),
]
module.exports = [{
    mode: 'development',
    entry: entry,
    output: {
        path: path.resolve(__dirname, "dist"),
    },
    module: webpackModule,
    plugins: plugin,
},{
    mode: 'development',
    entry: entry,
    output: {
        path: path.resolve(__dirname, "public"),
    },
    module: webpackModule,
    plugins: plugin,
}]
